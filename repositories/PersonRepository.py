from pymongo import MongoClient

from model import RouteConfig
from model.Person import Person
from utils import Constants
from utils.Configurator import configurator


class PersonRepository:
    client = None
    db = None
    collection_person = None

    def __init__(self):
        route_config = configurator.route_config #type: RouteConfig
        self.client = MongoClient(route_config.BASE_URL_MONGO, route_config.PORT_MONGO)
        self.db = self.client.db_person  # Getting a data-base
        self.collection_person = self.db.collection_person

    def get_person(self, id_person, id_organization):

        result = self.collection_person.find_one({"id_person": id_person, "id_organization": id_organization},
                                                 {"_id": 0, "signature": 0})

        person = Person(id_organization=id_organization, id_person=id_person)
        if result:
            return person
        return None

    def save_person(self, id_person, id_organization, signature):

        person = Person(id_organization=id_organization, id_person=id_person, signature=signature.tolist())
        ok = self.collection_person.update(
            {'$and': [{"id_person": id_person}, {"id_organization": id_organization}]},
            {"$set": vars(person)}, upsert=True)

        if ok:
            return person
        return None

    def edit_person(self, id_person, id_organization, signature):

        person = Person(id_organization=id_organization, id_person=id_person, signature=signature.tolist())
        ok = self.collection_person.update(
            {'$and': [{"id_person": id_person}, {"id_organization": id_organization}]},
            {"$set": vars(person)}, upsert=False)
        if ok['updatedExisting']:
            return person
        return None

    def delete_person(self, id_person, id_organization):
        person = Person(id_organization=id_person, id_person=id_organization)
        result = self.collection_person.remove({"id_person": id_person, "id_organization": id_organization},
                                               {"_id": 0, "signature": 0})

        if result['n'] > 0:
            return person
        return None

    def get_all_person(self, id_organization):
        person_list = self.collection_person.find({"id_organization": id_organization}, {"_id": 0})

        if person_list:
            return person_list
        return []
