import logging

import face_recognition
import numpy
from bson.json_util import dumps
from flask import Response

from repositories.PersonRepository import PersonRepository
from utils import Constants

mongo_service = PersonRepository()

logger = logging.getLogger('5ntp_face_api')


class CheckInService:
    id_organization = None  # type:int
    id_person = None  # type:int

    def __init__(self, id_organization=None, id_person=None):
        self.id_organization = id_organization
        self.id_person = id_person

    def get_person(self):

        person = mongo_service.get_person(id_person=self.id_person,
                                          id_organization=self.id_organization)
        if person:
            return self.__ok(person)
        return self.__err()

    def post_person(self, file):
        image = face_recognition.load_image_file(file)
        face_encoding_arr = face_recognition.face_encodings(image)
        if len(face_encoding_arr) == 1:
            person = mongo_service.save_person(id_person=self.id_person,
                                               id_organization=self.id_organization,
                                               signature=face_encoding_arr[0])
            if person:
                return self.__ok(person)
        return self.__err()

    def put_person(self, file):
        image = face_recognition.load_image_file(file)
        face_encoding_arr = face_recognition.face_encodings(image)
        if len(face_encoding_arr) == 1:
            person = mongo_service.edit_person(id_person=self.id_person,
                                               id_organization=self.id_organization,
                                               signature=face_encoding_arr[0])
            if person:
                return self.__ok(person)

        return self.__err()

    def delete_person(self):
        person = mongo_service.delete_person(id_person=self.id_person,
                                             id_organization=self.id_organization)

        if person:
            return self.__ok(person)
        return self.__err()

    def post_recognize(self, file):
        image = face_recognition.load_image_file(file)
        face_encoding_arr = face_recognition.face_encodings(image)
        mongo_person_list = list(mongo_service.get_all_person(id_organization=self.id_organization))

        mongo_signature_list = []
        for person in mongo_person_list:
            signature = numpy.array(person["signature"])
            mongo_signature_list.append(signature)
        person_list_recognized = []
        if len(face_encoding_arr) > 0:
            for face_encoding in face_encoding_arr:
                match_distances = face_recognition.face_distance(mongo_signature_list, face_encoding)
                lowest = 1
                person = None
                for index in range(len(match_distances)):
                    if match_distances[index] < 0.45 and match_distances[index] < lowest:
                        person = mongo_person_list[index]
                if person:
                    del person["signature"]
                    person_list_recognized.append(person)
        for person in person_list_recognized:
            print(person)
        return self.__ok_recognition(person_list_recognized)

    def __ok_recognition(self, result):
        return Response(response=dumps(result),
                        status=Constants.RESPONSE_CODE_SUCCESS,
                        mimetype='application/json')

    def __ok(self, result):
        if result.signature:
            del result.signature
        return Response(response=dumps(vars(result)),
                        status=Constants.RESPONSE_CODE_SUCCESS,
                        mimetype='application/json')

    def __err(self):
        return Response(response=dumps(dict(err='err')),
                        status=Constants.RESPONSE_CODE_ERROR,
                        mimetype='application/json')
