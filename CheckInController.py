import base64
import logging.config
import os

import requests
from bson.json_util import dumps
from builtins import print
from flask import Flask, request, Response, jsonify
from flask_jwt_extended import JWTManager, jwt_required
from flask_uploads import UploadSet, IMAGES
from werkzeug.datastructures import FileStorage

from services.CheckInService import CheckInService
from utils import Constants
from utils.Configurator import configurator
from utils.Constants import RESPONSE_CODE_ERROR, TEMP_IMAGE, RESPONSE_CODE_SUCCESS
from utils.ResponseHandler import ok, err
from utils.Tools import download_image, delete_image

photos = UploadSet('photos', IMAGES)
app = Flask(__name__)

current_path = os.path.dirname(os.path.abspath(__file__))

# Setup the Flask-JWT-Extended extension
app.config['JWT_SECRET_KEY'] = base64.b64decode("Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=")
app.config['JWT_ALGORITHM'] = "HS256"
jwt = JWTManager(app)

route_config = configurator.route_config


@app.route('/organization/<int:id_organization>/person/<int:id_person>', methods=['GET', 'POST', 'PUT', 'DELETE'])
@jwt_required
def save_person(id_organization, id_person):
    check_in_service = CheckInService(id_organization=id_organization, id_person=id_person)
    if request.method == 'GET':
        return check_in_service.get_person()

    elif request.method == 'POST' and 'file' in request.files:
        return check_in_service.post_person(request.files['file'])
    elif request.method == 'POST' and request.json and 'img' in request.json.keys():
        try:
            response = download_image(request.json['img'])
            print(response.content)
            with open(TEMP_IMAGE.format(id_organization, id_person), 'wb') as f:
                f.write(response.content)
            with open(TEMP_IMAGE.format(id_organization, id_person), 'rb') as f:
                file = FileStorage(f)
                result = check_in_service.post_person(file)
                delete_image(file.filename)
                return result
        except Exception as e:
            return Response(response=dumps(dict(err='err')), status=RESPONSE_CODE_ERROR, mimetype='application/json')

    elif request.method == 'PUT' and 'file' in request.files:
        return check_in_service.put_person(request.files['file'])
    elif request.method == 'PUT' and request.json and 'img' in request.json.keys():
        response = download_image(request.json['img'])
        with open(TEMP_IMAGE.format(id_organization, id_person), 'wb') as f:
            f.write(response.content)
        with open(TEMP_IMAGE.format(id_organization, id_person), 'rb') as f:
            file = FileStorage(f)
            result = check_in_service.put_person(file)
            delete_image(file.filename)
            return result

    elif request.method == 'DELETE':
        return check_in_service.delete_person()
    return Response(response=dumps(dict(err='err')), status=RESPONSE_CODE_ERROR, mimetype='application/json')


@app.route('/checkin/organization/<int:id_organization>', methods=['POST'])
@jwt_required
def recognize_person(id_organization):
    check_in_service = CheckInService(id_organization=id_organization)
    if request.method == 'POST' and 'file' in request.files:
        result = check_in_service.post_recognize(file=request.files['file'])
        return result

    return Response(response=dumps(dict(err='err')), status=RESPONSE_CODE_ERROR, mimetype='application/json')


@app.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        print("#########################",route_config.BASE_URL_CHECKIN)
        response = requests.post(route_config.BASE_URL_CHECKIN + "/login",
                                 data=str(request.json))
        return Response(response=dumps(response.json()), status=RESPONSE_CODE_SUCCESS, mimetype='application/json')
    return Response(response=dumps(dict(err='err')), status=RESPONSE_CODE_ERROR, mimetype='application/json')


@app.route('/configuration', methods=['GET', 'POST'])
@jwt_required
def configuration():
    if request.method == 'POST':
        response = configurator.set_configuration(request.json)
        if response:
            return ok(response)
    elif request.method == 'GET':
        response = configurator.get_configuration()
        if response:
            return ok(response)
    return err()


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        'ok': False,
        'message': 'Missing Authorization Header'
    }), 401


if __name__ == '__main__':
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    logging.config.fileConfig(os.path.join(current_path, "logging.conf"), disable_existing_loggers=False)
    app.run(host=Constants.PUBLIC_TRUSTFUL_HOST, threaded=True, debug=True, port=route_config.PORT_FLASK)
