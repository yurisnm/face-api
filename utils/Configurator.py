import pickle
import os

from model.RouteConfig import RouteConfig


class Configurator(object):
    route_config = None
    current_path = os.path.dirname(os.path.abspath(__file__))
    parent_path = os.path.abspath(os.path.join(current_path, os.pardir))

    def __init__(self):
        with open(os.path.join(self.parent_path, "CONFIG.pkl"), 'rb') as input:
            self.route_config = pickle.load(input)

    def set_configuration(self, json_config):
        try:
            base_url_mongo = json_config.get('BASE_URL_MONGO')
            base_url_checkin = json_config.get('BASE_URL_CHECKIN')
            port_flask = json_config.get('PORT_FLASK')
            port_checkin = json_config.get('PORT_CHECKIN')
            port_mongo = json_config.get('PORT_MONGO')
            self.route_config = RouteConfig(base_url_mongo, base_url_checkin, port_flask, port_checkin, port_mongo)
            print(vars(self.route_config))
            with open(os.path.join(self.parent_path, "CONFIG.pkl"), 'wb') as output:
                pickle.dump(self.route_config, output, pickle.HIGHEST_PROTOCOL)
                return self.route_config
        except:
            return None

    def get_configuration(self):
        with open(os.path.join(self.parent_path, "CONFIG.pkl"), 'rb') as input:
            self.route_config = pickle.load(input)
            return self.route_config


configurator = Configurator()
