from bson.json_util import dumps
from flask import Response

from utils import Constants


def ok(result):
    return Response(response=dumps(vars(result)),
                    status=Constants.RESPONSE_CODE_SUCCESS,
                    mimetype='application/json')


def err():
    return Response(response=dumps(dict(err='err')),
                    status=Constants.RESPONSE_CODE_ERROR,
                    mimetype='application/json')