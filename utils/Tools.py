import os

import requests


def download_image(url):
    return requests.get(url, allow_redirects=True)


def delete_image(path):
    os.remove(path)
    if os.path.exists(path) is False:
        return True
