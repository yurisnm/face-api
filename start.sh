#!/bin/sh

sudo docker build -t nokia-faceapi-checkin .
sudo docker rm -f nokia-faceapi-checkin
sudo docker run -d --name nokia-faceapi-checkin -p 5000:5000 --restart=always nokia-faceapi-checkin:latest
