class Person(object):
    id_person = None
    id_organization = None
    signature = None

    def __init__(self, id_organization=None, id_person=None, signature=None):
        self.id_organization = id_organization
        self.id_person = id_person
        self.signature = signature
