class RouteConfig(object):
    BASE_URL_MONGO = None
    BASE_URL_CHECKIN = None
    PORT_FLASK = None
    PORT_CHECKIN = None
    PORT_MONGO = None

    def __init__(self, base_url_mongo, base_url_checkin, port_flask, port_checkin, port_mongo):
        self.BASE_URL_MONGO = base_url_mongo
        self.BASE_URL_CHECKIN = base_url_checkin
        self.PORT_FLASK = port_flask
        self.PORT_CHECKIN = port_checkin
        self.PORT_MONGO = port_mongo
