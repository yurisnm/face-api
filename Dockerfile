FROM ubuntu:16.04

ENV GUNICORN_PORT=5000
ENV GUNICORN_MODULE=CheckInController
ENV GUNICORN_CALLABLE=app
ENV APP_PATH=/src/nokia-faceapi-checkin

RUN apt-get update
RUN apt-get install -y build-essential apt-utils

RUN apt-get install -y cmake git
RUN apt-get update && apt-get install -y python3-dev python3-numpy python3-pycurl

RUN apt-get install -y python3-pip
RUN pip3 install --upgrade pip

RUN apt-get install -y software-properties-common python-software-properties

RUN add-apt-repository ppa:mc3man/xerus-media
RUN apt-get update
RUN apt-get -y install ffmpeg

COPY requirements.txt $APP_PATH/requirements.txt
RUN pip3 install --no-cache-dir -r /src/nokia-faceapi-checkin/requirements.txt
RUN pip3 install APScheduler
RUN pip3 install dlib
RUN pip3 install face-recognition
RUN pip3 install face-recognition-models
RUN pip3 install pymongo

COPY . $APP_PATH
ADD . $APP_PATH
WORKDIR $APP_PATH
ENTRYPOINT $APP_PATH/entrypoint.sh

